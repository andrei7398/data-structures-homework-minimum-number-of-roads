// SFETCU IULIAN-ANDREI 312CD TEMA3-SD

#include <stdio.h>
#include <stdlib.h>

/* Depth first algorithm. */
void DFS(int k, int **A, int *visited, int n) {

    int i;

    visited[k]=1;

    for (i=0; i<n; i++)
        if (!visited[i] && A[k][i]==1)
            DFS(i, A, visited, n);
}

void Magic(char *input, char *output) {

  	FILE *fptr = fopen(input, "r");
  	FILE *out = fopen(output, "w");

  	int t, k;
  	fscanf(fptr, "%d", &k);

  	for (t=0; t<k; t++) {

  		int n, m, cdrum, csc;
	  	/* Get the number of villages,number of roads,
	     	and the cost for a road/village. */
	  	fscanf(fptr, "%d %d %d %d", &n, &m, &cdrum, &csc);

	  	int i, j, x, y;

	  	/* Adjency Matrix. */
	  	int **A = (int **)malloc(n*sizeof(int));
	  	for (i=0; i<n; i++)
	        A[i] = (int *)malloc(n*sizeof(int));

	  	/* Initialise Adjency Matrix elements with 0. */
	  	if (n != 0) {
	    	for (i=0; i<n; i++) {
	      		for (j=0; j<n; j++) {
	        		A[i][j] = 0;
	      		}
	      	}
	  	}

	  	/* Make Adjency Matrix. (x-1) and (y-1) because 
	  		the nodes start from 1, but our matrix from 0. */
	  	for (i=0; i<m; i++) {
	    	fscanf(fptr, "%d", &x);
	    	fscanf(fptr, "%d", &y);
	    	A[x-1][y-1] = 1;
	    	A[y-1][x-1] = 1;
	  	}

	  	/* This will hold the number of conex components. */
	  	int conex = 0;

	  	/* A vector that holds the visited villages. */
	  	int *visited = (int *)calloc(n,sizeof(int));

	  	for (i=0; i<n; i++) {
	  		if (!visited[i]) {
	  			DFS(i, A, visited, n);
	  			conex++;
	  		}
	  	}

	  	/* Our minimum cost. */
	  	int mincost = 0;

	  	/* If school cost is less, then we rebuilt every school. */
	  	if (csc <= cdrum)
	    	mincost = n*csc;

	  	/* If road cost is less, we rebuild a school for every
	     	conex component, then we rebuild the remaining roads
	     	in every component. */
	  	else if (csc > cdrum)
	    	mincost = conex*csc + (n-conex)*cdrum;

	  	/* Free the Adjency Matrix. */
	  	for (i=0; i<n; i++)
			free(A[i]);
	  	free(A);
		
	  	/* Output the minimum cost. */
	  	fprintf(out, "%d\n", mincost);
		}

	fclose(out);
  	fclose(fptr);
}

int main() {
	
	Magic("scoala.in", "scoala.out");

	return 0;
}